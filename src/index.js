import loaderUtils from 'loader-utils';
import defaultTheme from 'jsonresume-theme-flat';

module.exports = function (source) {
  const resume = JSON.parse(source);
  const options = Object.assign({}, loaderUtils.getOptions(this));

  const themePkg = options.theme || defaultTheme;
  return themePkg.render(resume);
};
