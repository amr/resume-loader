var  loaderUtils = require('loader-utils');

var defaultOptions = {
  theme: require('jsonresume-theme-flat')
};

module.exports = function (source) {
  var resume = JSON.parse(source);
  var options = Object.assign(
    {}, defaultOptions, loaderUtils.getOptions(this)
  );

  var themePkg = options.theme;
  return themePkg.render(resume);
};
